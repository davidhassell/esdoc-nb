#
# This simple script generates uml class diagrams as separate diagrams
# for all CIM2 classes
#
from esdoc_nb.mp.umlview import umlDiagram, ClassDoc
from esdoc_nb.mp.scripts import CIM_DOCS

if __name__ == "__main__":
    d = umlDiagram()
    for k in d.factory.classes:
        try:
            d.setClassDoc(k)
            d.plot(filebase='%s/class_uml/%s' % (CIM_DOCS, k))
            cd = ClassDoc(k)
            cd.plot(filebase='%s/class_docs/%s' % (CIM_DOCS, k))
        except:
            print 'Error for %s' % k
            raise