from esdoc_nb.mp.core.lib.factory import cimFactory
from esdoc_nb.mp.extended_vocab_view import VocabView
from esdoc_nb.mp.umlview import ClassDoc
from esdoc_nb.mp.scripts import CMIP6_DOCS

def domain_view(directory):
    """ This produces a complete view for each domain """
    domains = ['atmosphere', 'sea_ice', 'ocean']
    for d in domains:
        v = VocabView('cmip6', d, 1)
        v.plot(file_base=directory, fmt='png')

    ocean = ['ocean_keyproperties', 'ocean_timestepping_framework', 'ocean_advection', 'ocean_lateral_physics',
             'ocean_vertical_physics', 'ocean_uplow_boundaries', 'ocean_boundary_forcing']

    for p in ocean:
        v = VocabView('cmip6',p)
        v.plot(file_base='%s/ocean/' % CMIP6_DOCS, fmt='pdf', orientation='LR')

def process_view(directory):
    """ Here, an example for looking at just one process"""
    v = VocabView('cmip6', 'atmos_radiation')
    v.plot(file_base=directory, fmt='png')

def radiation_view(directory):
    """ And here, an example for just one class """
    factory = cimFactory()
    factory.add_extension_package('cmip6')
    d = ClassDoc('LwProperties', factory=factory)
    d.plot(filebase='%s/doc_radiation' % directory, dpi=300, fmt='png')

def ocean_enum_view(directory):
    """ And here, an example for just one class """
    factory = cimFactory()
    factory.add_extension_package('cmip6')
    d = ClassDoc('OceanTimesteppingProps', factory=factory)
    d.plot(filebase='%s/ocean_enums' % directory, dpi=300, fmt='png')
    #d = ClassDoc('OceanTimesteppingTra', factory=factory)
    #d.plot(filebase='%s/ocean_enums' % directory, dpi=300, fmt='png')


if __name__ == "__main__":

    domain_view('%s/vocabs/' % CMIP6_DOCS)
    radiation_view(CMIP6_DOCS)
    process_view('test_')
    ocean_enum_view('%s/ocean/' % CMIP6_DOCS)
