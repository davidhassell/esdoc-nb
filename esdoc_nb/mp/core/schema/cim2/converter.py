__author__ = 'BNL28'
from pseudo_meta import known as knownc
from pseudo_meta import knownCimClasses as kC
from pseudo_meta import isDocumentType


files = \
    {'activity_classes.py':'activity',
     'shared_classes.py':'shared',
     'shared_classes_time.py':'shared',
     'platform_classes.py':'platform',
     'data_classes.py':'data',
     'software_classes.py':'software'}


class_index = {}

for package in knownc:
    for c in knownc[package]:
        class_index[c] = package

enums = {'calendar_types':'shared',
                 'time_units':'shared',
                 'programming_language':'software',
                 'coupling_framework':'software',
                 'volume_units': 'platform',
                 'storage_systems': 'platform',
                 'ensembleTypes': 'activity',
                 'slicetimeUnits': 'shared',
                 'modelTypes': 'activity',
                 'roleCode': 'shared',
                 'period_date_types': 'shared',
                 'textCode': 'shared',
                 'forcingTypes':'activity'}


for e in enums:
    class_index[e]=enums[e]

for f in files:
    ff=open(f,'r')
    fo=open(f+'new','w')
    code = ff.readlines()
    for line in code:
        bits = line.split(',')
        newbits=[]
        for b in bits:
            if 'cim::' in b:
                if b.startswith("'"): b=' %s'%b
                ctype=b[7:-1]
                ntype='%s.%s'%(class_index[ctype],ctype)
                if ctype in kC:
                    if isDocumentType(ctype):
                        ntype='linked_to(%s)'%ntype
                newbits.append("'%s'"%ntype)
            else: newbits.append(b)
        newline = ','.join(newbits)
        fo.write(newline)
    ff.close()
    fo.close()








