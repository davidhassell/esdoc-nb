
def performance():
    """ Describes the properties of a performance of a configured model on a particular system/machine """
    return {
        'type': 'class',
        'base': None,
        'is_abstract': False,
        'pstr': ('%s (sypd:%s)', ('name', 'sypd',)),
        'properties': [
            ('name', 'str', '0.1',
                'Short name for performance (experiment/test/whatever)'),
            ('model', 'linked_to(science.model)', '1.1',
                'Model for which performance was tested'),
            ('platform', 'linked_to(platform.machine)', '1.1',
                'Platform on which performance was tested'),
            ('total_nodes_used', 'int', '0.1',
                'Number of nodes used'),
            ('compiler', 'str', '0.1',
                'Compiler used'),
            ('subcomponent_performance','platform.component_performance','0.1',
                'Describes the performance of each subcomponent'),
            ('sypd', 'float', '0.1',
                'Simulated years per wall-clock day'),
            ('chsy', 'float', '0.1',
                'Core-Hours per simulated year'),
            ('coupler_load', 'float', '0.1',
                'Percentage of time spent in coupler'),
            ('load_imbalance', 'float', '0.1',
                'Load imbalance'),
            ('io_load', 'float', '0.1',
                'Percentage of time spent in I/O'),
            ('memory_bloat','float','0.1',
                'Percentage of extra memory needed'),
            ('asypd', 'float', '0.1',
                'Actual simulated years per wall-clock day, all-in'),
            ('meta', 'shared.meta', '1.1',
                'Document metadata'),
            ]
    }


def component_performance():
    """ Describes the simulation rate of a component in seconds per model day """
    return {
        'type': 'class',
        'is_abstract': False,
        'base': None,
        'pstr': ('speed %s s/day',('speed',)),
        'properties':[
            ('component', 'software.software_component', '0.1',
                'Link to a CIM software component description'),
            ('component_name', 'str', '1.1',
                'Short name of component'),
            ('speed', 'float', '1.1',
                'Time taken to simulate one real day (s)'),
            # one of these  is needed:
            ('cores_used', 'int', '0.1',
                'Number of cores used for this component'),
            ('nodes_used', 'int', '0.1',
                'Number of nodes used for this component'),
            ],
        }


def partition():
    """A major partition (component) of a computing system (aka machine)"""
    return {
        'type': 'class',
        'base': None,
        'is_abstract': False,
        'properties': [
            ('name', 'str', '1.1',
                'Name of partition (or machine)'),
            ('institution', 'linked_to(shared.party)', '1.1',
                'Institutional location'),
            ('description', 'shared.cimtext', '0.1',
                'Textural description of machine'),
            ('model_number', 'str', '0.1',
                "Vendor's model number/name - if it exists"),
            ('vendor', 'linked_to(shared.party)', '0.1',
                'The system integrator or vendor'),
            ('when_used', 'time.time_period', '0.1',
                'If no longer in use, the time period it was in use'),
            ('partition', 'platform.partition', '0.N',
                'If machine is partitioned, treat subpartitions as machines'),
            ('compute_pools', 'platform.compute_pool', '1.N',
                'Layout of compute nodes'),
            ('storage_pools', 'platform.storage_pool', '0.N',
                'Storage resource available'),
            ('online_documentation', 'shared.online_resource', '0.N',
                'Links to documentation'),
            ]
        }


def machine():
    """A computer/system/platform/machine which is used for simulation."""
    return {
        'type': 'class',
        'base': 'platform.partition',
        'is_abstract': False,
        'properties': [
            ('meta', 'shared.meta', '1.1', 'Document description'),
            ]
        }
            
            
def compute_pool():
    """ Homogeneous pool of nodes within a computing machine. """
    return {
        'type': 'class',
        'base': None,
        'is_abstract': False,
        'properties': [
            ('name', 'str', '0.1',
                'Name of compute pool within a machine'),
            ('number_of_nodes', 'int', '0.1',
                'Number of nodes'),
            ('operating_system', 'str', '0.1',
                'Operating system'),
            ('cpu_type', 'str', '0.1',
                'CPU type'),
            ('model_number', 'str', '0.1',
                'Model/Board number/type'),
            ('memory_per_node', 'platform.storage_volume', '0.1',
                'Memory per node'),
            ('accelerator_type', 'str', '0.1',
                'Type of accelerator'),
            ('compute_cores_per_node', 'int', '0.1',
                'Number of CPU cores per node'),
            ('accelerators_per_node', 'int', '0.1',
                'Number of accelerator units on a node'),
            ('description', 'shared.cimtext', '0.1',
                'Textural description of pool'),
            ('interconnect', 'str', '0.1',
                'Interconnect used'),
            ],
        'derived': [
            ('total_cores', 'compute_cores_per_node * number_of_nodes'),
            ('total_memory', 'memory_per_node * number_of_nodes'),
            ]
        }


def storage_pool():
    """Homogeneous storage pool on a computing machine"""
    return {
        'type': 'class',
        'is_abstract': False,
        'base': None,
        'properties': [
            ('name', 'str', '1.1',
                'Name of storage pool'),
            ('volume_available', 'platform.storage_volume', '1.1',
                'Storage capacity'),
            ('vendor', 'linked_to(shared.party)', '0.1',
                'Vendor of the storage unit'),
            ('description', 'shared.cimtext', '0.1',
                'Description of the technology used'),
            ('type', 'platform.storage_systems', '0.1',
                'Type of storage'),
            ]
        }


def storage_volume():
    """ Volume and units """
    return {
        'type': 'class',
        'base': None,
        'is_abstract': False,
        'doc': 'Volume with units ',
        'pstr': ('%s %s',('volume','units')),
        'properties': [
            ('volume', 'int', '1.1', 'Numeric value'),
            ('units', 'platform.volume_units', '1.1', 'Volume units'),
            ]
        }


def volume_units():
    """Appropriate storage volume units."""
    return {
        'type': 'enum',
        'is_open': False,
        'members': [
            ('GB', 'Gigabytes (1000^3)'),
            ('TB', 'Terabytes (1000^4)'),
            ('PB', 'Petabytes (1000^5)'),
            ('EB', 'Exabytes (1000^6)'),
            ('TiB', 'Tebibytes (1024^4)'),
            ('PiB', 'Pebibytes (1024^5)'),
            ('EiB', 'Exbibytes (1024^6)'),
            ],
        }


def storage_systems():
    """ Controlled vocabulary for storage  types (including filesystems)."""
    return {
        'type': 'enum',
        'is_open': False,
        'members': [
            ('Lustre', 'Lustre parallel file system'),
            ('GPFS', 'IBM GPFS (also known as IBM Spectral Scale'),
            ('isilon', 'The EMC scaleout NAS solution'),
            ('NFS', 'Generic Network File System'),
            ('S3', 'Object file system exposing the AWS S3 interface'),
            ('PanFS', 'Panasas Parallel File system'),
            ('Other Disk', 'Other disk based file system'),
            ('Tape - MARS', 'Tape storage system using ECMWF MARS '),
            ('Tape - MASS', 'Tape storage system using Met Office MASS'),
            ('Tape - Castor', 'Tape storage sytsem using CERN Castor'),
            ('Tape - Other', 'Other tape based system'),
            ],
        }
