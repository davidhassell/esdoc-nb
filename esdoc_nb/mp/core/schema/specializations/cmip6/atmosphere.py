AUTHOR_GUIDE = 'URL on wordpress site of useful info for authors "CMIP6 specilaisations author guide". This page will be a generic guide on how to fill in a REALM, PROCESS, SUB_PROCESS, SUB_PROCESS_DETAILS, etc. http://cmip6.specialisation.guide/process.html'

ID = 'cmip6.atmosphere'

CONTACT = None

AUTHORS = None

TYPE = 'cim.2.science.scientific_realm'

# ====================================================================
# REAL: PROPERTIES
# ====================================================================
DESCRIPTION = None

NAME = 'atmosphere'

REALM = 'atmosphere'

OVERVIEW = None

GRID = ['atmosphere_grid']

KEY_PROPERTIES = ['atmosphere_key_properties']

PROCESSES = ['atmosphere_cloud_scheme',
             'atmosphere_cloud_simulator',
             'atmosphere_dynamical_core',
             'atmosphere_gravity_waves',
             'atmosphere_microphysics_precipitation',
             'atmosphere_radiation',
             'atmosphere_turbulence_convection',
         ]
