__author__ = 'eguil'
version = '0.0.1'

#
# CMIP6 ocean time stepping framework CV

ID = 'cmip6.ocean.timestepping_framework'

PROPERTIES = {  
   
    'type': 'science.process',
   
    # ----------------------------------------------------------------
    # OVERVIEW
    # ----------------------------------------------------------------
    'short_name': 'Ocean time stepping framework',
    'description': 'Properties of ocean time stepping framework within the ocean component',
    
    'references': [],
    'details': ['timestepping_attributes',
                'timestepping_tracers',
                'barotropic_solver',
                'barotropic_momemtum'],

    
    # ----------------------------------------------------------------
    # TOP-LEVEL DETAILS
    # ----------------------------------------------------------------    
    'timestepping_attributes': {
        'description': 'Properties of time stepping in ocean',
        'short_name': 'Properties of time stepping in ocean',
        'time steps': (
            'int', '1.1',
            'Ocean time step in seconds'),
        'diurnal_cycle': ('ENUM:diurnal_cycle_types', '1.1','closed'),        
    },


    # ----------------------------------------------------------------
    # SUB-PROCESSES
    # ----------------------------------------------------------------
    
    # ----------------------------------------------------------------
    # DETAILS
    # ----------------------------------------------------------------
    'timestepping_tracers': {
        # Scientific context and properties
        'short_name': 'Timestepping for tracers',
        'description': 'Properties of timestepping for tracers in ocean',
        'timestepping_tracers_scheme':('ENUM:ocean_timestepping_types','1.1'),
        },

    'barotropic_solver': {
        'short_name': 'Barotropic solver in ocean',
        'description': 'Properties of barotropic solver in ocean',
        'timestepping_tracers_scheme':('ENUM:ocean_timestepping_types','1.1'),
        },

    'barotropic_momentum': {
        'short_name': 'Barotropic momentum in ocean',
        'description': 'Properties of barotropic momentum in ocean',
        'timestepping_tracers_scheme':('ENUM:ocean_timestepping_types','1.1'),
        },
}

ENUMS = {
#
# CV for enumerated lists
#
    'diurnal_cycle_types' : {
        'description': 'Types of diurnal cycle resolution in ocean',
        'members': [
            ('None','No diurnal cycle in ocean'),
            ('Via coupling','Diurnal cycle via coupling frequency'),
            ('Specific treatment', 'Specific treament'),
            ]
        },
    
    'ocean_timestepping_types' : {
        'description': 'Type of timestepping scheme in ocean',
        'members': [
            ('Leap-frog + Asselin filter', 'Leap-frog scheme with Asselin filter'),
            ('Leap-frog + Periodic Euler backward solver', 'Leap-frog scheme with Periodic Euler backward solver'),
            ('Predictor-corrector', 'Predictor-corrector scheme'),
            ('AM3-LF (ROMS)', 'AM3-LF used in ROMS'),
            ('Forward-backward', 'Forward-backward scheme'),
            ]
        },
    }
