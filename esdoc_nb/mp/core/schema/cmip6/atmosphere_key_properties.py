ID = 'cmip6.atmosphere.key_properties'

CIM = '2.0'

TYPE = 'science.key_properties'

CONTACT = 'Charlotte Pascoe (e-mail)'

AUTHORS = 'Charlotee Pascoe, David Hassell'

DATE = '2016-03-11'

VERSION = ''

# ====================================================================
# PROPERTIES
# ====================================================================
PROPERTIES = {
    
    # ----------------------------------------------------------------
    # MANIFEST
    # ----------------------------------------------------------------   
    'short_name': 'Atmosphere key properties',
    'description': 'Key properties of the atmosphere',
    
    'details': ['general',
                'top_insolation_solar_constant',
                'top_insolation_orbital_parameters',
                'top_insolation_ozone',
                'orography',
            ],

    # ----------------------------------------------------------------
    # DETAILS
    # ----------------------------------------------------------------
    'general': {
        'short_name': '',
        'description': '',
         'model_family': (
            'ENUM:model_family_type', '1.1',
            'Type of atmospheric model.'),
        'basic_approximations': (
            'ENUM:basic_approximations_attributes', '1.N',
            'Basic approximations made in the atmosphere.',)
        'volcanoes_implementation': (
            'ENUM:volcanoes_implementation_method', '1.1',
            'How volcanic effects are modeled in the atmosphere.'),
    },
    
    'top_insolation_solar_constant': {
        'short_name': '',
        'description': '',
         'type': (
            'ENUM:top_insolation_solar_constant_type', '1.1', 
            'Time adaptation of the solar constant.'),
        'fixed_value', (
            'float', '0.1', 
            'If the solar constant is fixed, enter the value of the solar constant (W m-2).'),
        'transient_characteristics': (
            'str', '1.1',
            'solar constant transient characteristics (W m-2)'),
        'dynamical_core_top_boundary_condition': (
            'ENUM:dynamical_core_top_boundary_condition', '1.1', 
            'Type of boundary layer at the top of the model.'),
    },
    
    'top_insolation_orbital_parameters': {
        'short_name': '',
        'description': '',
        'type': (
            'ENUM:top_insolation_orbital_parameters_type', '1.1',
            'DESCRIPTION'),
        'fixed_reference_date', (
            'int', '1.1',
            'fixed orbital parameters reference date (yyyy)'),
        'solar_constant_transient_characteristics': (
            'str', '1.1',
            'transient orbital parameters characteristics'),
        'computation_method':  (
            'ENUM:top_insolation_orbital_parameters_computation_method', '1.1',
            'Method used for computing orbital parameters.')
    },

    'top_insolation_ozone': {
        'short_name': 'Top of atmosphere insolation impact on ozone',
        'description': 'Top of atmosphere insolation impact on ozone',
        'top_insolation_ozone': (
            'bool', '1.1',
            'Impact of top of atmosphere insolation on stratospheric ozone'),
    },
    
    'orography': {
        'short_name': '',
        'description': '',
         'type': (
            'ENUM:orography_type', '1.1',
            'Time adaptation of the orography.',),
        'changes': (
            'ENUM:orography_changes', '1.N',
            'If the orography type is modified describe the time adaptation changes.'),
    },


    # ----------------------------------------------------------------
    # EXTENT
    # ----------------------------------------------------------------
    
    # ----------------------------------------------------------------
    # RESOLUTION
    # ----------------------------------------------------------------

    # ----------------------------------------------------------------
    # TUNING
    # ----------------------------------------------------------------

    # ----------------------------------------------------------------
    # CONSERVATION
    # ----------------------------------------------------------------
}

# ====================================================================
# ENUMERATIONS
# ====================================================================
ENUMERATIONS = {

    'model_family_type': {
        'short_name': 'Type of atmospheric model.'
        'description': 'Type of atmospheric model.'
        'members': [
            ('AGCM', 'Atmospheric General Circulation Model'),
            ('ARCM', 'Atmospheric Regional Climate Model'),
            ('other', None),
        ]
    },
    
    'basic_approximations_attributes': {
        'short_name': 'Basic approximations made in the atmosphere.',
        'description': 'Basic approximations made in the atmosphere.',
        'members': [
            ('primitive equations', None),
            ('non-hydrostatic', None),
            ('anelastic', None),
            ('Boussinesq', None),
            ('hydrostatic', None),
            ('quasi-hydrostatic', None),
            ('other', None),
        ],
    },

    'top_insolation_solar_constant_type': {
        'short_name': 'Time adaptation of the solar constant.'
        'description': 'Time adaptation of the solar constant.'
        'members': [
            ('fixed', None),
            ('transient', None),
        ]
    },

    'dynamical_core_top_boundary_condition': {
        'short_name': 'Type of boundary layer at the top of the model.'
        'description': 'Type of boundary layer at the top of the model.'
        'members': [
            ('sponge layer', None),
            ('radiation boundary condition', None),
            ('other', None),
        ]
    },

    'top_insolation_orbital_parameters_computation_method': { 
        'short_name': 'Method used for computing orbital parameters.'
        'description': 'Method used for computing orbital parameters.'
        'members': [
            ('Berger 1978', None),
            ('Laskar 2004', None),
            ('Other', None),
        ]
    },


    'orography_type': {
        'short_name': 'Time adaptation of the orography.',
        'description': 'Time adaptation of the orography.',
        'members': [
            ('present-day', None),
            ('modified', None),
        ]
    },

    'orography_changes': {
        'short_name': 'If the orography type is modified describe the time adaptation changes.'
        'description': 'If the orography type is modified describe the time adaptation changes.'
        'members': [
            ('related to ice sheets', None),
            ('related to tectonics', None),
            ('modified mean', None),
            ('modified variance if taken into account in model (cf gravity waves)', None),
        ]
    },

    'volcanoes_implementation_method': {
        'short_name': 'How volcanic effects are modeled in the atmosphere.'
        'description': 'How volcanic effects are modeled in the atmosphere.'
        'members': [
            ('high frequency solar constant anomaly', None),
            ('stratospheric aerosols optical thickness', None),
            ('other', None),
            ('none', None),
        ]
    },

}
