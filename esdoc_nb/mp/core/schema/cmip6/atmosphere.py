ID = ''

TYPE = 'science.scientific_realm'

CIM = ''

CONTACT = ''

AUTHORS = ''

DATE = ''

VERSION = ''

# ====================================================================
# PROPERTIES
# ====================================================================
PROPERTIES = {

    # ----------------------------------------------------------------
    # MANIFEST
    # ----------------------------------------------------------------
    'short_name': None,              
    'description': None,             
 
    'name': 'atmosphere',
    'realm', 'atmosphere',

    'overview': '',

    'grid': ['atmopshere_grid'],

    'key_properties': ['atmosphere_key_properties'],

    'processes': ['cloud_scheme',
                  'cloud_simulator',
                  'dynamical_core',
                  'gravity_waves',
                  'microphysics_precipitation',
                  'radiation',
                  'turbulence_convection',
              ],
}

# ====================================================================
# ENUMERATIONS
# ====================================================================
ENUMERATIONS = {}
