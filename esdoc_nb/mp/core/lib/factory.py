from definitions import classmap, class_packages, enum_packages, extension_points
from meta import function2dict, get_from_modules, gather_base_heirarchy, gather_base_properties, decode_extensions
from uml import umlClass, umlEnum, umlProperty
from datetime import datetime
import uuid


class cimFactory(object):

    """ This class defines the CIM eco system and the internal classes, and
    allows one to build class instances."""

    # Try and insulate the notebook world from the esdoc-mp world by ensuring
    # that in esdoc land, everything that was abc_def is now AbcDef.

    def __init__(self, default=True):

        """ CIM eco system constructor. Default is to construct the
        basic CIM. If not default, construct as an empty factory."""

        self.classes = {}
        self.enums = {}
        self.packages = {}

        if default:
            self.extend(class_packages)
            self.extend(enum_packages)

    def build(self, klass, initialise_metadata=False, author=None):
        """ Makes and returns an instance of klass, if
         initialise_author and klass is a document, add a
         metadata record using author if provided."""
        if klass in self.classes:
            v = umlClass(self.classes[klass], self.fullset)
            if initialise_metadata and v.isDocument:
                meta = meta_setup(self.classes, author=author)
                v.meta = meta
            return constrain_instance(v, self)
        elif klass in self.enums:
            v = umlEnum(self.enums[klass])
            return v
        else:
            raise ValueError('Cannot find constructor for %s in factory' % klass)

    def __contains__(self, klass):
        """ Allows one to determine if klass is in the CIM system """
        if klass not in self.classes and klass not in self.enums:
            return False
        return True

    def base(self, klass, follow=True):
        """For a given cim type, find base classes"""
        if klass in self.classes:
            d = self.classes[klass]
        elif klass in self.enums:
            d = self.enums[klass]
        else:
            raise ValueError('Base class requested for unknown klass : %s' % klass)
        return gather_base_heirarchy(d, self.fullset)

    def find_associates(self, klass, documents_only=False, get_base=True, get_properties=True):
        """ For a given CIM class, klass, find all other classes which
        are associated *from* that class.
        If get_base, then include all base classes,
        If get_properties, then include all property classes.
        if documents_only, only return document associates.
        """
        extras = []
        if klass in self.enums: return extras
        assert klass in self.classes, 'What to do with %s?' % klass
        model = self.classes[klass]
        if get_base and model['base']:
            extras += self.base(model['cimType'])
        if get_properties and 'properties' in model:
            for p in model['properties']:
                klass = str(p[1])
                if klass in self and klass not in extras:
                    if not documents_only or (documents_only and self.isdoc(klass)):
                        extras.append(klass)
        return extras

    def isdoc(self, klass):
        """ Convenience method to return whether or not a klass has metadata, ie
        conforms to the CIM2 document stereotype, without needing to build the
        class first."""
        if klass not in self: return False
        if klass in self.enums: return False
        if self.classes[klass]['is_abstract']: return False
        result = 'meta' in [p[0] for p in gather_base_properties(self.classes[klass], self.fullset)]
        return result

    def extend(self, package_list):
        """  Extend to include building domain specific classes, e.g. cmip6
        :param package_list: A list of domain specific class constructors.
        """
        constructors, packages = get_from_modules(package_list)

        self._load_constructors(constructors)

        for p in packages:
            if p in self.packages:
                self.packages[p] += packages[p]
            else:
                self.packages[p] = packages[p]

        self.fullset = self.classes.copy()
        self.fullset.update(self.enums)

    def _load_constructors(self, constructors):
        """ Load constructors into internal class and enum dictionaries """
        # At the moment classes and enums are mixed
        for k in constructors:
            if constructors[k]['type'] == 'class':
                self.classes[k] = constructors[k]
            elif constructors[k]['type'] == 'enum':
                self.enums[k] = constructors[k]

    def add_package_by_functions(self, package_name, package_functions):
        """ Add one package and its constructors from the basic function definitions.
        Used for cmip6 type package extensions.
        :param package_name: A new package
        :param package_constructors: It's corresponding constructors
        :return: None
        """
        # TODO Deprecate
        constructors = {}
        self.packages[package_name] = []
        for f in package_functions:
            key, c = function2dict(f)
            constructors[key] = c
            self.packages[package_name].append(key)
        self._load_constructors(constructors)

        self.fullset = self.classes.copy()
        self.fullset.update(self.enums)

    def add_extension_package(self, package_name):
        """Add extension package using compact notation"""
        names, constructors = decode_extensions(package_name)
        self.packages[package_name] = names
        self._load_constructors(constructors)
        self.fullset = self.classes.copy()
        self.fullset.update(self.enums)


def constrain_instance(obj, factory):
    """Once an obj has been instantiated, any constraints should be applied.
    Currently understands constraints of the form
            ('description','cardinality','1.1')
            ('description','value','Unchangeable string')
            ('description','hidden')
            ('property','includes',[list of classes])
    """
    understood = ['cardinality','hidden','value','includes']
    if obj.constraint_set:
        for p in obj.attributes:
            if p in obj.constraint_set:
                constraint = obj.constraint_set[p]
                if constraint[0] not in understood:
                    raise ValueError('Unrecognised constraint %s' % str(constraint))
                if constraint[0] == 'hidden':
                    del obj.attributes[p]
                elif constraint[0] == 'cardinality':
                    existing = list(obj.attributes[p].uml_attribute)
                    existing[2] = constraint[1]
                    obj.attributes[p] = umlProperty(existing)
                elif constraint[0] == 'include':
                    prop = getattr(obj, p)
                    assert not prop.single_valued
                    assert isinstance(constraint[1], list)
                    for x in constraint[1]:
                        prop.append(factory.build(x))
                elif constraint[0] == 'value':
                    existing = list(obj.attributes[p].uml_attribute)
                    obj.attributes[p] = umlProperty(existing, value=constraint[1])
    return obj



def make_cim_instance(cimtype, factory=None, initialise_metadata=True):
    """ Make a cim instance,
    either using the default factory class (factory = None),
    or using whatever factory is currently in use (with the provided factory).
    In the latter case this will use extension points to change the class
    actually delivered, if appropriate """
    if factory is None:
        factory = cimFactory()
    if cimtype in factory.fullset:
        extended = False
        # If appropriate, build using the class at the extension point
        for p in extension_points:
            if p in factory.packages:
                if cimtype in extension_points[p]:
                    cimtype = extension_points[p][cimtype]
                    extended = True
        instance = factory.build(cimtype, initialise_metadata=initialise_metadata)
        return instance
    else:
        raise ValueError('Factory cannot instantiate unknown non abstract class/enum [%s][%s]' %
                         (cimtype, sorted(factory.fullset)) )


def convert_from_pyesdoc(doc, target_type=None):
    """ Converts a pyesdoc class instance into a pseudo_mp umlClass instance
    :param doc: a document encoded as a pyesdoc class instance
    :param target_type: if known, the target type
    :return doc: a notebook pseudo_mp umlClass instance
    """
    # We normally know the target_type once we're dealing with the attributes
    # of a class_instance, it's only the "top" class instance we would parse.
    if not target_type:
        target_type = doc.type_key.split('.')[-1]
    target = make_cim_instance(target_type)
    # First check if this is an enum string value, if so, just set the value and leave
    if isinstance(doc, str):
        target.set(doc)
        return target
    # OK, now we know it's a class we're dealing with:
    for a in target.attributes:
        umlp = target.attributes[a]
        attr = umlp.name
        value = getattr(doc, attr)
        if value is None:
            setattr(target, attr, value)
        elif umlp.target not in classmap:
            if umlp.single_valued:
                setattr(target, attr, convert_from_pyesdoc(value, umlp.target))
            else:
                setattr(target, attr, [convert_from_pyesdoc(v, umlp.target) for v in value])
        else:
            setattr(target, attr, value)
    return target


def makeCimInstanceFromSet(cimtype, cimset):
    """ Makes a cim instance of cimtype from within a set of possible cimtypes: cimset """
    cimtype = package_split(cimtype)[1]
    assert cimtype in cimset,'%s not in %s' % (cimtype,sorted(cimset.keys()))
    constructor = cimset[cimtype]
    assert 'type' in constructor
    assert constructor['type'] in ['class', 'enum']
    if constructor['type'] == 'class':
        return umlClass(constructor)
    elif constructor['type'] == 'enum':
        return umlEnum(constructor)


def makeQuickText(content):
    ''' Given some plain text, turn it into a cimtext object '''
    c = make_cim_instance('Cimtext')
    tc = make_cim_instance('TextCode')
    tc.set('plaintext')
    c.content_type = tc
    c.content = content
    return c

def imeta_setup(constructor_set, source='nb-uml'):
    """ Set up internal metadata """
    meta = umlClass(constructor_set['DocMetaInfo'], constructor_set)
    meta.id = str(uuid.uuid1())
    meta.source = source
    meta.create_date = datetime.now().isoformat(' ')
    meta.update_date = meta.create_date
    meta.version = 1
    return meta

def meta_setup(constructor_set, author=None):
    """ Setup a metadata instance for a CIM document
    :param constructor_set: Set of possible CIM entities
    :param author: Author of this metadata description
    """

    meta = umlClass(constructor_set['Meta'], constructor_set)
    imeta = imeta_setup(constructor_set)
    if author:
        imeta.author = author
        meta.metadata_author = author

    meta._internal_metadata = imeta

    return meta


