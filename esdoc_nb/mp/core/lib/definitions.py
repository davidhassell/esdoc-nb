from .. schema.cim2 import \
    activity_classes as activity_c, \
    shared_classes as shared_c, \
    time as shared_time_c, \
    software_classes as software_c, \
    platform_classes as platform_c, \
    science_classes as science_c, \
    data_classes as data_c, \
    drs_entities as drs_c, \
    designing_classes as designing_c, \
    software_enums as software_e, \
    science_enums as science_e

#
# PYTHON CLASSES supported
#

classmap = {
    'int': [int, ],
    'str': [str, unicode],
    'datetime': [str, ],
    'float': [float, ],
    'bool': [bool, ],
    'text': [str, unicode]
}

#
# KNOWN STANDARD CIM classes
#

class_packages = {
    'activity': activity_c,
    'shared': shared_c,
    'time': shared_time_c,
    'software': software_c,
    'platform': platform_c,
    'science': science_c,
    'data': data_c,
    'drs': drs_c,
    'designing': designing_c,
}

enum_packages = {'software': software_e,
                 'science': science_e,
                 }
#
# Formal extension packages, e.g. cmip6
#
from ..schema.cmip6 import get_items

extension_packages = {'cmip6': get_items()}


# These are the classes where vocabulary extensions *can* takeover ... if implemented
#

extension_points = {'cmip6':
                        {'Model': 'Cmip6Model',
                         'ensemble': 'Cmip6Ensemble'}
                    }