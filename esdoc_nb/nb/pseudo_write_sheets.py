import os
import unittest
from collections import OrderedDict

from xlwt import Workbook, easyxf

import esdoc_nb.mp.core.lib.uml
import esdoc_nb.mp.mp_tests as mp
from pseudo_spreadsheet import read_spreadsheet, BINDING

__author__ = 'BNL28'
DEBUG_LEVEL = 0

DESIGNING = OrderedDict([
    ('NumericalExperiment',
        [('name',), ('long_name',), ('canonical_name',), ('keywords',), ('description',),
         ('responsible_parties', 'role', 'party'), ('references',),
         ('meta', 'metadata_author', 'uid'),
         ('related_experiments',),
         ('requirements', 'TemporalConstraint', 'EnsembleRequirement', 'NumericalRequirement', 'ForcingConstraint')]),
    ('Party',
        [('name',), ('organisation',), ('address',), ('email',), ('url',), ('meta', 'uid')]),
    ('Citation',
        [('doi',), ('short_cite',), ('context',), ('citation_str',), ('url',), ('abstract', )]),
    ('OnlineResource',
        [('name', ), ('linkage', ), ('protocol', ), ('description', )]),
    ('NumericalRequirement',
     [('name', ),('long_name',), ('canonical_name',), ('keywords',), ('description',),
         ('responsible_parties','role','party'), ('references',), ('meta','uid'),
         ('is_conformance_requested',), ('additional_requirements',)]),
    ('ForcingConstraint',
        [('name', ),('long_name',), ('canonical_name',), ('keywords',), ('description',),
         ('responsible_parties','role','party'), ('references',), ('meta','uid'),
         ('is_conformance_requested',), ('forcing_type',)]),
    ('TemporalConstraint',
        [('name', ),('long_name',), ('canonical_name',), ('keywords',), ('description',),
         ('responsible_parties','role','party'), ('references',), ('meta','uid'),
         ('is_conformance_requested',), ('required_duration', 'length', 'units'),
         ('required_calendar', 'standard_name'), ('start_date' ,'value', 'offset'),
         ('start_flexibility', 'length', 'units')]),
    ('EnsembleRequirement',
         [('name', ),('long_name',), ('canonical_name',), ('keywords',), ('description',),
         ('responsible_parties','role','party'), ('references',), ('meta','uid'),
         ('is_conformance_requested',), ('ensemble_type',), ('minimum_size',),
         ('ensemble_member',)]),
        ])

class NamedOrderedDict(OrderedDict):
    """ Monkey patched ordered dict to add a special attribute.
    Could do this without a special class and properties, but this
    is so the monkey patch is visible """

    def __init__(self,*args,**kwargs):
        super(NamedOrderedDict, self).__init__(*args,**kwargs)
        self.__celltype = None

    def get_celltype(self):
        return self.__celltype

    def set_celltype(self, value):
        self.__celltype = value

    cell_type = property(get_celltype, set_celltype)


class Registries(object):

    """Gathers re-usable entries for binding"""

    def __init__(self):
        self.data = {}
        for b in BINDING:
            self.data[b] = {}

    def update_with(self, doc):
        """ Parse a document to find entities for the registry """

        def __update(d):
            key = BINDING[d.cimType]
            value = getattr(d, key)
            if value not in self.data[d.cimType]:
                self.data[d.cimType][value] = d

        if doc.cimType in BINDING:
            __update(doc)
            return

        for a in doc.attributes:
            attr = getattr(doc, a)
            if isinstance(attr, esdoc_nb.mp.core.lib.uml.umlClass):
                if attr.cimType in BINDING:
                    __update(attr)
                else:
                    self.update_with(attr)
            elif isinstance(attr, list):
                for aa in attr:
                    if isinstance(aa, esdoc_nb.mp.core.lib.uml.umlClass):
                        self.update_with(aa)


class Sheet(object):
    """ This class instantiates a metamodel for a CIM spreadsheet """
    def __init__(self, cimtype, view=DESIGNING):
        """
        :param cimtype: Type (and name) of the sheet
        :param view: Type of view (defines the sist of columns, as defined below)
        :return: instance

        Columns consist of a list of attribute names (and if necessary, sub-attributes) from the
        list of available attributes for that specific cimtype, which are required for this spreadsheet.

        E.g. for OnlineResource, we'd have [('name',),('linkage',),('protocol',),('description',)]
             for Party, we'd have [('name',),('organisation',),('address',),('email',),('url',),('meta','uid')]
        and something with a responsible_parties attribute would have
            [ ... ('responsible_parties','role','party')] but if there are multiple parties with the same
            role we'd expect to see multiple party columns under a merged 'responsible_parties' cell.

        """
        self.cimtype = cimtype
        self.columns = view[cimtype]
        self.internal_columns = []
        self.rows = []
        self.sub_columns = []
        self.registries = {}
        self.easy_entities = []
        for k in mp.classmap:
            self.easy_entities += mp.classmap[k]
        for c in self.columns:
            if len(c) == 1:
                self.internal_columns.append(c)
                self.sub_columns.append('')
            else:
                for sc in c[1:]:
                    self.internal_columns.append(c)
                    self.sub_columns.append(sc)

    @property
    def number_of_rows(self):
        return len(self.rows)

    @property
    def number_of_columns(self):
        return len(self.columns)

    def add_entity(self, entity):
        """ Add a cim entity to the sheet as a row.
        :param entity: A cim entity conforming to sheet type
        :return:
        """

        self.rows.append([self.__add_major_cell(c, entity) for c in self.columns])

    def __add_major_cell(self, heading, entity):
        """
        :param heading: This is the official heading expected for this cell expressed as a
                        tuple (header, sub-header1, sub-header2). There may be no sub-headers.
                        Sub-headers are either attributes of the entity, so the cell content
                        will be the value of the sub-attribute of the header attribute of the
                        entity, or they are different sub-classes, so the cell contents will
                        be the values of attributes of the entity which fall into that sub-class.

        :param entity: This is the entity from which the content for this cell must be
                        extracted.
        :return: An ordered dictionary which has keys which are the sub-headings, and a list of
                 columns for each of those sub-headings. Each column could include multiple rows.


        """

        def __get_subclasses(h, a):
            """ Handle the cells of document names in a sub-class list
            :param h: heading
            :param a: attribute
            :return: a dictionary of sub-class lists with document names
            """
            data = NamedOrderedDict()
            data.cell_type = 'subclass'
            for c in h[1:]:
                data[c] = []
                for x in a:
                    if x.remote_type == c:
                        data[c].append(x.name)
            return data

        def __get_attrs(h, a, sv):

            if sv:
                data = NamedOrderedDict()
                data.cell_type = 'attributes'
                for c in h[1:]:
                    newa = getattr(a,c)
                    newu = a.attributes[c]
                    if newu.islink and not newu.single_valued:
                        data[c] = []
                        for x in newa:
                            data[c].append(x.name)
                    else:
                        data[c] = __handle_cell(newu, newa)
                data = [data]
            else:
                data = []
                for x in a:
                    data.append(__get_attrs(h, x, True)[0])
            return data

        def __handle_cell(u, aa):
            if not aa:
                return ''
            if u.single_valued:
                aa = [aa, ]
            r = []
            for a in aa:
                if u.islink:
                    v = a.name
                elif u.target in BINDING:
                    v = getattr(a, BINDING[u.target])
                    if u.target not in self.registries:
                        self.registries[u.target] = {}
                    if v not in self.registries[u.target]:
                        self.registries[u.target][v] = a
                elif u.target in mp.classmap:
                    v = a
                elif u.target == 'Cimtext':
                    v = a.content
                elif isinstance(a, esdoc_nb.mp.core.lib.uml.umlEnum):
                    v = str(a)
                else:
                    raise NotImplementedError('%s, %s ' % (a.__class__, a))
                r.append(v)
            return r

        # Start by determining the nature of the cell
        attr = getattr(entity, heading[0])
        umlp = entity.attributes[heading[0]]
        if len(heading) == 1:
            # simple cell, but could still be a list
            od = __handle_cell(umlp, attr)
        else:
            if umlp.single_valued:
                looking_for_attributes = heading[1] in attr.attributes
            else:
                if len(attr) >= 1:
                    looking_for_attributes = heading[1] in attr[0].attributes
                else:
                    looking_for_attributes = False
                    return ['']
            if looking_for_attributes:
                # sub-attributes
                od = __get_attrs(heading, attr, umlp.single_valued)
            else:
                # sub-classes
                assert not umlp.single_valued, 'Unexpected single valued attribute %s %s %s' % (umlp, heading, attr)
                assert umlp.islink, 'expect sub-classes to be document types\n %s\n %s\n %s' % (umlp, heading, attr)
                # There may be multiple values expected here, aggregate to these first
                # The keys of this ordered dictionary correspond to the sub-headings, multiple cells may
                # appear in each however, which means we need to know about the number of columns needed
                od = __get_subclasses(heading, attr)

        return od

    def write(self, work_book):
        """
        :param work_book: An xlwt workbook
        :return: modified workbook
        """

        def __write_row (top_row, bottom_row, column_sizes, column_indices, content, style=None):
            """ Write a list of <content> cells of size <column_sizes> at <column_indices> """
            # sheet.write_merge(top_row, bottom_row, left_column, right_column, 'Merged Cell')

            for i, c in enumerate(content):
                myleft = column_indices[i]
                myright = myleft + column_sizes[i] - 1
                __write_cell(top_row, bottom_row, myleft, myright, c, style)

        def __write_list_of_cells(t, b, l, this_cell):
            """ Write a possible sequence of cells along row from current column"""
            if isinstance(this_cell, list):
                for kk, item in enumerate(this_cell):
                    __write_cell(t, b, l+kk, l+kk, item)
            else:
                __write_cell(t, b, l, l, this_cell)

        def __write_cell(tr, br, cleft, cright, cell, style=None):
            if DEBUG_LEVEL:
                print 'Writing at [%s,%s,%s,%s] value <%s>' % (tr, br, cleft, cright, cell)
            if style is None:
                style = normal_style
            if isinstance(cell, list):
                raise ValueError("Programming bug %s" % cell)
            if tr != br or cleft != cright:
                wbsheet.write_merge(tr, br, cleft, cright, cell, style)
            else:
                wbsheet.write(tr, cleft, cell, style)

        def __accumulate(a_list, starting_at=0):
            """ Used to accumulate indices for cells """
            b_list = list(a_list)
            b_list.insert(0, starting_at)
            total = 0
            for x in b_list[:-1]:
                total += x
                yield total

        wbsheet = work_book.add_sheet(self.cimtype)

        # at this point we don't know how many actual spreadsheet rows and columns we have,
        # because each row and column from the uber cells could have a different number of cells
        # arising from proliferation of document references. However, every uber-row knows
        # how many rows it needs, it's just the columns we have to look across everything to do.

        # Each row is currently a list of cells corresponding to the major column heading, we now
        # need to find out how many columns sit under each major column heading. To do that we
        # have to parse each cell and discriminate between extra rows and extra columns.

        # start with the headers:

        column_major_size = [max(1, len(c)-1) for c in self.columns]
        minors = sum(column_major_size)
        column_minor_size = [1 for i in range(minors)]
        assert sum(column_minor_size) == minors

        column_row_size = [[1 for ij in range(i)] for i in column_major_size]

        row_depth = [1 for i in range(len(self.rows))]

        for ri, row in enumerate(self.rows):

            assert len(row) == len(self.columns)

            for i, cell in enumerate(row):

                # Expecting three kinds of cells: lists, strings, and named dictionaries.
                # Cells will only have extra rows if there are lists which include multiple named dictionaries.
                # Cells will have extra columns if they contain lists with more than one element.
                if isinstance(cell, list):
                    if isinstance(cell[0], NamedOrderedDict):
                        row_depth[ri] = max(row_depth[ri], len(cell))
                        # Loop over the sub-rows:
                        for rcell in cell:
                            assert rcell.cell_type == 'attributes', rcell
                            assert len(rcell) == column_major_size[i], 'Length problem %s %s %s ' % \
                                                                       (i, rcell, column_major_size[i])
                            # Are there extra columns in this cell above the number expected?
                            clen = [1 for ii in range(column_major_size[i])]
                            for ii, k in enumerate(rcell):
                                clen[ii] = len(rcell[k])
                            for ii, c in enumerate(clen):
                                column_row_size[i][ii] = max(column_row_size[i][ii],c)
                    else:
                        # This should pick up lists of related docs which generate columns
                        column_row_size[i][0] = max(column_row_size[i][0], len(cell))
                elif cell != '':
                    # This should pickup named dictionaries  which generate columns
                    assert len(cell) == column_major_size[i], ' %s %s %s' % (i,cell,column_major_size[i])
                    for ii, k in enumerate(cell):
                        column_row_size[i][ii] = max(column_row_size[i][ii], len(cell[k]))

        ii = -1
        for ci, mc in enumerate(column_row_size):
            for ac in mc:
                ii += 1
                column_minor_size[ii] = ac
            column_major_size[ci] = sum(mc)

        column_major_index = list(__accumulate(column_major_size))
        column_minor_index = list(__accumulate(column_minor_size))
        row_depth_index = list(__accumulate(row_depth, starting_at=2))

        bold_style = easyxf('font: bold True; pattern: pattern solid, fore-color blue_gray;')
        normal_style = easyxf('font: bold False;')
        __write_row(0, 0, column_major_size, column_major_index, [cc[0] for cc in self.columns], style=bold_style)
        __write_row(1, 1, column_minor_size, column_minor_index, [cc for cc in self.sub_columns], style=bold_style)


        for ri, row in enumerate(self.rows):

            top = row_depth_index[ri]
            bottom = row_depth_index[ri] + row_depth[ri] - 1

            for ci, cell in enumerate(row):

                # Are there extra rows in this cell?
                if DEBUG_LEVEL:
                    print 'working on <%s>' % cell
                if len(cell) == 1:
                    rcell = cell[0]
                    if isinstance(rcell, NamedOrderedDict):
                        for ii, k in enumerate(rcell):
                            left = column_major_index[ci]+ii
                            __write_list_of_cells(top, bottom, left, rcell[k])
                    else:
                        left = column_major_index[ci]
                        __write_list_of_cells(top, bottom, left, rcell)
                else:
                    for jj, rcell in enumerate(cell):
                        if isinstance(rcell, NamedOrderedDict):
                            for ii, k in enumerate(rcell):
                                left = column_major_index[ci]+ii
                                __write_list_of_cells(top+jj, top+jj, left, rcell[k])
                        else:
                            if isinstance(cell, NamedOrderedDict):
                                rcell = cell[rcell]
                            left = column_major_index[ci]
                            __write_list_of_cells(top, bottom, left+jj, rcell)



class TestWriter(unittest.TestCase):

    def setUp(self):
        filename = '~/Downloads/CMIP6Experiments.xlsx'
        self.filename = os.path.expanduser(filename)
        self.docset = read_spreadsheet(self.filename)
        ofile = '~/Downloads/testing.xls'
        self.ofile = os.path.expanduser(ofile)

    def test_data_available(self):
        """ Check test data is available"""
        self.assertTrue(os.path.exists(self.filename),'Test data [%s] not available' % self.filename)

    def test_initial(self):
        testing = ['Party',]
        for t in testing:
            print 'writing ', t
            s = Sheet(t)
            for d in self.docset:
                if d.cimType == t:
                    print d
                    s.add_entity(d)
                    print 'Added ', d
            wb = Workbook()
            s.write(wb)
            wb.save(self.ofile)

    def test_next(self):
        testing = ['NumericalExperiment',]
        for t in testing:
            print 'writing ', t
            s = Sheet(t)
            for d in self.docset:
                if d.cimType == t:
                    print d
                    s.add_entity(d)
                    print 'Added ', d
            wb = Workbook()
            s.write(wb)
            wb.save(self.ofile)

    def test_it_all(self):
        sheets = {}
        registries = Registries()
        for d in self.docset:
            t = d.cimType
            if t not in sheets:
                sheets[t] = Sheet(t)
            print 'Adding %s to %s ' % (d, t)
            sheets[t].add_entity(d)
            registries.update_with(d)
        for b in registries.data:
            sheets[b] = Sheet(b)
            for v in registries.data[b]:
                sheets[b].add_entity(registries.data[b][v])
                print 'Adding %s to %s ' % (v, b)
        wb = Workbook()
        for t in sheets:
            sheets[t].write(wb)

        wb.save(self.ofile)


if __name__ == "__main__":
    unittest.main()











